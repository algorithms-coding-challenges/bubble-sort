const output = document.getElementById('output');

const bubbleSort = (array) => {
  if (array.length <= 1) {
    return array;
  }

  for (i = 0; i < array.length - 1; i++) {
    for (j = 0; j < array.length - 1 - i; j++) {
      if (array[j] > array[j + 1]) {
        let temp = array[j];
        array[j] = array[j + 1];
        array[j + 1] = temp;
      }
    }
  }

  return array;
}

output.innerText = bubbleSort(array = [1, 4, 2, 345, 123, 43, 32, 5643, 63, 123, 43, 2, 55, 1, 234, 92, 3]);
